package slice

import (
	"reflect"
	"testing"
)

func TestForUnion_positive(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"Two", "Seven"}

	expectedOutput := []string{"One", "Three", "Two", "Seven"}

	operations := &StringSlice{}

	output := operations.Union(input1, input2)
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForUnion_negative(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"Two", "Seven"}

	unexpectedOutput := []string{"One", "Two", "Seven"}

	operations := &StringSlice{}

	output := operations.Union(input1, input2)
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForIntersection_positive(t *testing.T) {

	input1 := []string{"One", "Three", "Seven"}
	input2 := []string{"Two", "Seven"}

	expectedOutput := []string{"Seven"}

	operations := &StringSlice{}

	output := operations.Intersection(input1, input2)
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForIntersection_negative(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"Two", "Seven"}

	unexpectedOutput := []string{"Two", "Seven"}

	operations := &StringSlice{}

	output := operations.Intersection(input1, input2)
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestIfEqual_positive(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"Two", "Seven"}

	expectedOutput := false

	operations := &StringSlice{}

	output := operations.IsEqual(input1, input2)
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestIfEqual_negative(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"One", "Three"}

	unexpectedOutput := false

	operations := &StringSlice{}

	output := operations.IsEqual(input1, input2)
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestIfSubset_positive(t *testing.T) {

	input1 := []string{"One", "Three", "Nine", "Seven"}
	input2 := []string{}

	expectedOutput := true

	operations := &StringSlice{}

	output := operations.IsSubset(input1, input2)
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestIfSubset_negative(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"Two", "Seven"}

	unexpectedOutput := true

	operations := &StringSlice{}

	output := operations.IsSubset(input1, input2)
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestIfProperSet_positive(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"One", "Three"}

	expectedOutput := false

	operations := &StringSlice{}

	output := operations.IsProperSet(input1, input2)
	if !reflect.DeepEqual(output, expectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestIfProperSet_negative(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"One"}

	unexpectedOutput := false

	operations := &StringSlice{}

	output := operations.IsProperSet(input1, input2)
	if reflect.DeepEqual(output, unexpectedOutput) {
		t.Errorf("Test Failed")
	}

}

func TestForComplementOfSet_positive(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"Two", "Seven"}

	operations := &StringSlice{}

	UniversalSet := operations.Union(input1, input2)

	expectedOutput1 := []string{"Two", "Seven"}

	output1 := operations.ComplementOfSet(UniversalSet, input1)
	if !reflect.DeepEqual(output1, expectedOutput1) {
		t.Errorf("Test Failed")
	}

	expectedOutput2 := []string{"One", "Three"}

	output2 := operations.ComplementOfSet(UniversalSet, input2)
	if !reflect.DeepEqual(output2, expectedOutput2) {
		t.Errorf("Test Failed")
	}

}

func TestForComplementOfSet_negative(t *testing.T) {

	input1 := []string{"One", "Three"}
	input2 := []string{"Two", "Seven"}
	operations := &StringSlice{}
	UniversalSet := operations.Union(input1, input2)

	unexpectedOutput1 := []string{"One", "Two"}

	output1 := operations.ComplementOfSet(UniversalSet, input1)
	if reflect.DeepEqual(output1, unexpectedOutput1) {
		t.Errorf("Test Failed")
	}

	unexpectedOutput2 := []string{"One", "Two"}

	output2 := operations.ComplementOfSet(UniversalSet, input2)
	if reflect.DeepEqual(output2, unexpectedOutput2) {
		t.Errorf("Test Failed")
	}

}
