package slice

import (
	"reflect"
	"sort"
)

type SetOperations interface {
	Intersection(slice1, slice2 []string) []string
	Union(slice1, slice2 []string) []string
	IsEqual(slice1, slice2 []string) bool
	IsSubset(slice1, slice2 []string) bool
	IsProperset(slice1, slice2 []string) bool
	ComplementOfSet(slice1, slice2 []string) []string
}

type StringSlice struct{}

func (s *StringSlice) Intersection(slice1, slice2 []string) []string {

	elements := make(map[string]bool)
	for _, value := range slice1 {
		elements[value] = true
	}

	var commonELements []string
	for _, value := range slice2 {
		if elements[value] {
			commonELements = append(commonELements, value)
		}
	}

	return commonELements
}

func (s *StringSlice) Union(slice1, slice2 []string) []string {

	slice := append(slice1, slice2...)

	emptyMap := map[string]bool{}

	newSlice := []string{}
	for _, element := range slice {
		if !emptyMap[element] {
			newSlice = append(newSlice, element)
			emptyMap[element] = true
		}
	}
	return newSlice
}

func (s *StringSlice) IsEqual(slice1, slice2 []string) bool {

	sort.Strings(slice1)
	sort.Strings(slice2)

	result := reflect.DeepEqual(slice1, slice2)
	return result

}

func (s *StringSlice) IsSubset(slice1, slice2 []string) bool {

	elements := make(map[string]bool)
	for _, value := range slice1 {
		elements[value] = true
	}

	for _, value := range slice2 {
		if !elements[value] {
			return false
		}
	}
	return true
}

func (s *StringSlice) IsProperSet(slice1, slice2 []string) bool {

	if s.IsEqual(slice1, slice2) {
		return false
	} else if s.IsSubset(slice1, slice2) {
		return true
	}
	return false
}

func (s *StringSlice) ComplementOfSet(UniversalSet, slice []string) []string {

	elements := make(map[string]bool)
	for _, value := range slice {
		elements[value] = true
	}

	var Complement []string
	for _, value := range UniversalSet {
		if !elements[value] {
			Complement = append(Complement, value)
		}

	}
	return Complement
}
