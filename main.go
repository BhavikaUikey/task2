package main

import (
	"fmt"
	"main/slice"
)

func main() {

	slice1 := []string{"One", "Three", "Seven", "Nine", "Five"}
	slice2 := []string{"Seven"}

	operations := &slice.StringSlice{}

	intersection := operations.Intersection(slice1, slice2)
	fmt.Println("Intersection: ", intersection)

	fmt.Print("\n")

	union := operations.Union(slice1, slice2)
	fmt.Println("Union: ", union)

	fmt.Print("\n")

	IsEqual := operations.IsEqual(slice1, slice2)
	fmt.Println("The slices are equal?\n", IsEqual)

	fmt.Print("\n")

	IsSubset := operations.IsSubset(slice1, slice2)
	fmt.Println("Is slice2 a subset of slice1?\n", IsSubset)

	fmt.Print("\n")

	IsProperSet := operations.IsProperSet(slice1, slice2)
	fmt.Println("Is slice2 a proper subset of slice1?\n", IsProperSet)

	fmt.Print("\n")

	ComplementOfSlice1 := operations.ComplementOfSet(union, slice1)
	fmt.Println("Complement of slice1 is: ", ComplementOfSlice1)

	ComplementOfSlice2 := operations.ComplementOfSet(union, slice2)
	fmt.Println("Complement of slice1 is: ", ComplementOfSlice2)

}
